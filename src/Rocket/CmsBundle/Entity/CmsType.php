<?php

namespace Rocket\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="cms_types", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={ "class" }),
 * }, indexes= {
 *     @ORM\Index(columns={ "class" }),
 * })
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class CmsType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", nullable=false, unique=true)
     */
    protected $class;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CmsContent", mappedBy="cmsType")
     */
    protected $cmsContents;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cmsContents = new ArrayCollection();
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set Class
     *
     * @param string $class
     * @return \Rocket\CmsBundle\Entity\CmsType
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * Get CMS Contents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCmsContents()
    {
        return $this->cmsContents;
    }

    /**
     * Add CMS Content
     *
     * @param \Rocket\CmsBundle\Entity\CmsContent $cmsContent
     * @return \Rocket\CmsBundle\Entity\CmsType
     */
    public function addCmsContent(CmsContent $cmsContent)
    {
        $this->cmsContents[] = $cmsContent;
        return $this;
    }

    /**
     * Remove CMS Content
     *
     * @param \Rocket\CmsBundle\Entity\CmsContent $cmsContent
     * @return \Rocket\CmsBundle\Entity\CmsType
     */
    public function removeCmsContent(CmsContent $cmsContent)
    {
        $this->cmsContents->removeElement($cmsContent);
        return $this;
    }

    /**
     * Get Created At
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set Created At
     *
     * @param \DateTime $createdAt
     * @return \Rocket\CmsBundle\Entity\CmsType
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get Updated At
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set Updated At
     *
     * @param \DateTime $updatedAt
     * @return \Rocket\CmsBundle\Entity\CmsType
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->class;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setCreatedAtAsNow()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setUpdatedAtAsNow()
    {
        $this->updatedAt = new \DateTime();
    }
}
