<?php

namespace Rocket\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Templating\EngineInterface;
use Rocket\CmsBundle\Entity\CmsType;
use Rocket\CmsBundle\Entity\CmsContent;

class CmsContentType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'constraints' => array(
                    new Assert\NotBlank(array(
                        'message' => 'backend.cmscontent.error.name'
                    ))
                )
            ))
            ->add('uri', 'text', array(
                'required' => false,
            ))
            ->add('requireAuth')
            ->add('locale')
        ;
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rocket\CmsBundle\Entity\CmsContent',
            'label_prefix' => 'backend.cmscontent.'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rocket_cmsbundle_cmscontenttype';
    }
}
