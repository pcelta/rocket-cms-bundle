<?php

namespace Rocket\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Rocket\CmsBundle\Entity\CmsType;
use Rocket\CmsBundle\Entity\CmsContent;
use Rocket\CmsBundle\Form\CmsContentType;
use Rocket\CmsBundle\Content\Exception\CmsContentException;

/**
 * CmsContent controller.
 *
 * @Route("/cmscontent")
 */
class CmsContentController extends Controller
{

    /**
     * Lists all CmsContent entities.
     *
     * @Route("/")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $cmsContents = $this->getDoctrine()->getRepository('CmsBundle:CmsContent')->createQueryBuilder('cc')
            ->addSelect('ct')
            ->join('cc.cmsType', 'ct')
            ->where('cc.version = 0')
            ->orderBy('cc.cmsType')
            ->addOrderBy('cc.locale')
            ->getQuery()
            ->getResult();
        $cmsTypes = $this->getDoctrine()->getRepository('CmsBundle:CmsType')->findAll();

        return array(
            'entities' => $cmsContents,
            'cmsTypes' => $cmsTypes,
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
        );
    }

    /**
     * Displays a form to create a new CmsContent entity.
     *
     * @Route("/new/{id}")
     * @Method("GET")
     * @Template()
     */
    public function newAction(CmsType $cmsType)
    {
        $cms = $this->get('rocket.cms');

        $cmsContent = new CmsContent();
        $content = $cms->instantiate($cmsContent, $cmsType->getClass());

        $cmsContentForm = $this->createForm(new CmsContentType(), $cmsContent);
        $serializedDataForm = $this->createForm($content->getForm());

        return array(
            'cmsType' => $cmsType,
            'cmsContent' => $cmsContent,
            'cmsContentForm' => $cmsContentForm->createView(),
            'content' => $content,
            'serializedDataForm' => $serializedDataForm->createView(),
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
            'cms_content_form_theme' => $this->container->getParameter('cms.content_form_theme'),
            'cms_serialized_data_form_theme' => $this->container->getParameter('cms.serialized_data_form_theme'),
        );
    }

    /**
     * Creates a new CmsContent entity.
     *
     * @Route("/create/{id}")
     * @Method("POST")
     * @Template("CmsBundle:CmsContent:new.html.twig")
     */
    public function createAction(CmsType $cmsType)
    {
        $request = $this->getRequest();
        $cms = $this->get('rocket.cms');
        $em = $this->getDoctrine()->getManager();

        $cmsContent = new CmsContent();
        $cmsContent->setVersion(0);
        $cmsContent->setCmsType($cmsType);
        $content = $cms->instantiate($cmsContent, $cmsType->getClass());
        $cmsContentForm = $this->createForm(new CmsContentType(), $cmsContent);
        $serializedDataForm = $this->createForm($content->getForm());
        $cmsFilesDir = $this->container->getParameter('cms.files_path');

        $translator = $this->get('translator');
        $session = $this->get('session');
        $flashBag = $session->getFlashBag();

        try {
            $serializedDataForm->bind($request);
            $data = $content->processForm($serializedDataForm, $cmsFilesDir);
            $cmsContent->setSerializedData($data);
            $cmsContentForm->bind($request);

            if ($cmsContentForm->isValid() && $serializedDataForm->isValid()) {
                $em->persist($cmsContent);
                $em->flush();
                $flashBag->add('success', $translator->trans('general.success_save'));

                return $this->redirect($this->generateUrl('rocket_cms_cmscontent_show', array('id' => $cmsContent->getId())));
            }
        } catch (CmsContentException $e) {
            $session = $this->get('session');
            $session->getFlashBag()->add('error', $e->getMessage());
        }

        return array(
            'cmsType' => $cmsType,
            'cmsContent' => $cmsContent,
            'cmsContentForm' => $cmsContentForm->createView(),
            'content' => $content,
            'serializedDataForm' => $serializedDataForm->createView(),
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
            'cms_content_form_theme' => $this->container->getParameter('cms.content_form_theme'),
            'cms_serialized_data_form_theme' => $this->container->getParameter('cms.serialized_data_form_theme'),
        );
    }

    /**
     * Finds and displays a CmsContent entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     */
    public function showAction(CmsContent $cmsContent)
    {
        $cms = $this->get('rocket.cms');
        $content = $cms->instantiate($cmsContent);

        // The page content is the only exception that can't be rendered because it includes the frontend template
        if ($cmsContent->getCmsType()->getClass() == 'Rocket\CmsBundle\Content\PageContent') {
            $serializedData = null;
        } else {
            $serializedData = $content->render();
        }

        return array(
            'entity' => $cmsContent,
            'serializedData' => $serializedData,
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
            'cms_frontend_template' => $this->container->getParameter('cms.frontend_template'),
        );
    }

    /**
     * Displays a form to edit an existing CmsContent entity.
     *
     * @Route("/{id}/edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction(CmsContent $cmsContent)
    {
        $cms = $this->get('rocket.cms');
        $cmsType = $cmsContent->getCmsType();
        $content = $cms->instantiate($cmsContent, $cmsType->getClass());

        $cmsContentForm = $this->createForm(new CmsContentType(), $cmsContent);
        $serializedDataForm = $this->createForm($content->getForm(), $cmsContent->getSerializedData());

        return array(
            'cmsType' => $cmsType,
            'cmsContent' => $cmsContent,
            'cmsContentForm' => $cmsContentForm->createView(),
            'content' => $content,
            'serializedDataForm' => $serializedDataForm->createView(),
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
            'cms_content_form_theme' => $this->container->getParameter('cms.content_form_theme'),
            'cms_serialized_data_form_theme' => $this->container->getParameter('cms.serialized_data_form_theme'),
        );
    }

    /**
     * Creates a new CmsContent entity.
     *
     * @Route("/update/{id}")
     * @Method("POST")
     * @Template("CmsBundle:CmsContent:edit.html.twig")
     */
    public function updateAction(CmsContent $cmsContent)
    {
        $request = $this->getRequest();
        $cms = $this->get('rocket.cms');
        $em = $this->getDoctrine()->getManager();
        $cmsContentRepository = $em->getRepository('CmsBundle:CmsContent');
        $cmsType = $cmsContent->getCmsType();
        $newCmsContent = new CmsContent();
        $newCmsContent->setVersion(0);
        $newCmsContent->setCmsType($cmsType);
        $content = $cms->instantiate($newCmsContent, $cmsType->getClass());
        $cmsContentForm = $this->createForm(new CmsContentType(), $newCmsContent);
        $serializedDataForm = $this->createForm($content->getForm(), $request->request->get($content->getForm()->getName()));
        $cmsFilesDir = $this->container->getParameter('cms.files_path');

        $translator = $this->get('translator');
        $session = $this->get('session');
        $flashBag = $session->getFlashBag();

        try {
            $serializedDataForm->bind($request);
            $data = $content->processForm($serializedDataForm, $cmsFilesDir);
            $newCmsContent->setSerializedData($data);
            $cmsContentForm->bind($request);

            if ($cmsContentForm->isValid() && $serializedDataForm->isValid()) {
                $cmsContentRepository->updateVersions($cmsContent);
                $em->persist($newCmsContent);
                $em->flush();
                $flashBag->add('success', $translator->trans('general.success_save'));

                return $this->redirect($this->generateUrl('rocket_cms_cmscontent_show', array('id' => $newCmsContent->getId())));
            }
        } catch (CmsContentException $e) {
            $session = $this->get('session');
            $session->getFlashBag()->add('error', $e->getMessage());
        }

        return array(
            'cmsType' => $cmsType,
            'cmsContent' => $cmsContent,
            'cmsContentForm' => $cmsContentForm->createView(),
            'content' => $content,
            'serializedDataForm' => $serializedDataForm->createView(),
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
            'cms_content_form_theme' => $this->container->getParameter('cms.content_form_theme'),
            'cms_serialized_data_form_theme' => $this->container->getParameter('cms.serialized_data_form_theme'),
        );
    }
}
