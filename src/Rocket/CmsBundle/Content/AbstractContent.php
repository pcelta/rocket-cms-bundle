<?php

namespace Rocket\CmsBundle\Content;

use Rocket\CmsBundle\Entity\CmsContent;
use Symfony\Component\Form\Form;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\Container;

/**
 * @author Fernando Carletti <fcarletti@rocket-internet.com.br>
 * @author Maykon Diógenes <maykon.diogenes@rocket-internet.com.br>
 */
abstract class AbstractContent
{
    /**
     * @var \Rocket\CmsBundle\Entity\CmsContent
     */
    protected $cmsContent;

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    /**
     * @var \Symfony\Component\Form\FormFactory
     */
    protected $formFactory;

    /**
     * @var \Twig_Environment $templating
     */
    protected $templating;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    protected $translator;

    /**
     * @param \Rocket\CmsBundle\Entity\CmsContent $cmsContent
     * @param \Symfony\Component\DependencyInjection\Container $container
     */
    public function __construct(CmsContent $cmsContent, Container $container)
    {
        $this->cmsContent = $cmsContent;
        $this->container = $container;

        $this->formFactory = $this->container->get('form.factory');
        $this->templating = $this->container->get('templating');
        $this->entityManager = $this->container->get('doctrine.orm.default_entity_manager');
        $this->translator = $this->container->get('translator');
    }

    /**
     * @param array $data External data (will be merged with serialized data)
     * @return string Rendered content
     */
    public function render($data = array())
    {
        $data = array_merge($this->cmsContent->getSerializedData(), $data);
        $content = $this->templating->render($this->getTemplateName(), $data);

        return $content;
    }

    /**
     * @return \Symfony\Component\Form\AbstractType
     */
    abstract public function getForm();

    /**
     * @return bool
     */
    public function requireAuth()
    {
        return $this->cmsContent->getRequireAuth();
    }

    /**
     * @return string
     */
    abstract public function getTemplateName();

    /**
     * Return the form template path
     *
     * @return  string
     */
    public function formTemplate()
    {
        return null;
    }

    /**
     * Process form contents
     *
     * @param \Symfony\Component\Form\Form $form
     * @internal param array $data
     *
     * @return  array
     */
    public function processForm(Form $form, $cmsFilesDir = null)
    {
        $data = $form->getData();

        return $data;
    }

    /**
     * Get class name
     *
     * @return string
     */
    protected function getClassName()
    {
        $className = get_class($this);
        $className = preg_replace('/.*\\\([a-zA-Z]+)$/', '$1', $className);

        return $className;
    }

    /**
     * Return cms content entity
     *
     * @return \Rocket\CmsBundle\Entity\CmsContent
     */
    public function getEntity()
    {
        return $this->cmsContent;
    }
}
