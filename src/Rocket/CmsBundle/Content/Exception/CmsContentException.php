<?php

namespace Rocket\CmsBundle\Content\Exception;

/**
 * Cms content exception
 */
class CmsContentException extends \Exception
{
}
